 function MyApp(link){
  // console.log('init function');
  // https://www.json-generator.com/#
  var fetchedData = fetch(link).then(function(response) {
    return response.json();
  }).then(data => {
    if (data.every(checkObj)) {
      renderInterface(data);
    } else {
      alert('Error, diferent type obj');
  };
    

  });

};
MyApp('http://www.json-generator.com/api/json/get/bUuBhzcCRe?indent=2');
var arrayFirst = [];
var arraySecond = [];
var arrayThird = [];
function renderInterface( data ){
  data.forEach(checkAll);
  console.log(data);
  console.log(arrayFirst);
  console.log(arraySecond);
  console.log(arrayThird);
}

function checkObj (item) {
 return (item.hasOwnProperty('favoriteFruit','balance','age','eyeColor','gender','isActive'))
}

function addLi (item,nameUL){
  var newLi = document.createElement('li');
  newLi.innerHTML = item.name;
  document.getElementById(nameUL).appendChild(newLi);
}

function normalMoney (money) {
  var lastPos = money.indexOf(".");
  var newMoney = money.substring(0, lastPos);
  newMoney = parseInt((newMoney.replace(/\$|\,/gi, "")),10);
  return newMoney;
};

function checkAll (item) {
  if (item.favoriteFruit == "banana") {
     arrayFirst.push(item);
     addLi(item,'firstUl')
  }

  if (normalMoney(item.balance) > 2000 && item.age > 25) {
    arraySecond.push(item);
    addLi(item,'secondUl')
  }

  if (item.eyeColor === "blue" && item.gender === "female" && item.isActive === false) {
    arrayThird.push(item);
    addLi(item,'thirdUl')
  }
};

function changeLink(){
  var link = document.getElementById('text').value;
  var pattern = /https?:\/\/www\.json\-generator\.com\/api\/json\/get\/[\w\d]+\?indent\=2/g;
  if (link.match(pattern)){
    clearApp();
    MyApp(link);
  } else {
    alert('Error, bad link');
};
};

function clearApp(){
  arrayFirst = [];
  arraySecond = [];
  arrayThird = [];
 var uls = document.querySelectorAll('ul');
 for (var i=0; i < uls.length; i++) {
  uls[i].innerHTML = "";
 }
};
// Задача:
// Написать ф-ю которая принимает на вход обьект с сервера и
// разбить его на 3 массива по параметрам описаным ниже.
// + бонус вывести каждый список на экран
// + бонус 2 сделать поле инпута куда вставить ссылку с json-generator
// для перерендера списка по клику на кнопку
// + бонус 3 если вставить не валидную ссылку выводить ошибку

// # = условие -> вывод
// array 1 = fruit == banana -> name,
// array 2 = balance > 2000, age > 25
// array 3 = eyeColor === blue, gender === female, isActive === false
